#include "words.h"
#include <sds/sds.h>

#include <stdlib.h>

sds random_name(void) {
  const char* adj1 = ADJECTIVES[rand() % ADJECTIVES_SIZE];
  const char* adj2 = ADJECTIVES[rand() % ADJECTIVES_SIZE];
  const char* noun = NOUNS[rand() % NOUNS_SIZE];
  sds name = sdsnew(adj1);
  name = sdscat(name, "_");
  name = sdscat(name, adj2);
  name = sdscat(name, "_");
  name = sdscat(name, noun);
  return name;
}
