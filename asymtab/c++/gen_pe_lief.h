#ifndef H_GEN_PE_LIEF
#define H_GEN_PE_LIEF

#include <libasymtab/asymtab.h>

asymtab_t* gen_from_pe_lief(char* path);

#endif//H_GEN_PE_LIEF