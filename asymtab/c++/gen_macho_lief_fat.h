#ifndef H_GEN_MACHO_LIEF_FAT
#define H_GEN_MACHO_LIEF_FAT

#include <libasymtab/asymtab.h>

struct fat_asymtab {
	asymtab_t** symtabs;
	char** output_paths;
	size_t size;
};

struct fat_asymtab gen_from_macho_lief_fat(char* input_path, char* output_path);

#endif//H_GEN_ELF_LIEF