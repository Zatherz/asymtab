#include <LIEF/LIEF.hpp>

extern "C" {
	#include "gen_elf_lief.h"
	#include "../cli.h"
  	#include <libasymtab/asymtab.h>
  	#include <libanalog/akit_colors.h>

  	static const char* log_id = ANALOG_COLOR_ID("elf_lief");

  	static const char* type_to_str(LIEF::ELF::ELF_SYMBOL_TYPES type) {
  		switch(type) {
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_NOTYPE: return "type:notype";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_OBJECT: return "type:object";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_FUNC: return "type:func";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_SECTION: return "type:section";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_FILE: return "type:file";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_COMMON: return "type:common";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_TLS: return "type:tls";
		//case LIEF::ELF::ELF_SYMBOL_TYPES::STT_GNU_IFUNC: return "gnu_ifunc"; // duplicate of LOOS???
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_LOOS: return "type:loos/gnu_ifunc";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_HIOS: return "type:hios";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_LOPROC: return "type:loproc";
		case LIEF::ELF::ELF_SYMBOL_TYPES::STT_HIPROC: return "type:hiproc";
		default: return "type:unknown";
  		}
  	}

  	static const char* binding_to_str(LIEF::ELF::SYMBOL_BINDINGS binding) {
  		switch(binding) {
		case LIEF::ELF::SYMBOL_BINDINGS::STB_LOCAL: return "binding:local";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_GLOBAL: return "binding:global";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_WEAK: return "binding:weak";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_LOOS: return "binding:loos/gnu_unique";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_HIOS: return "binding:hios";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_LOPROC: return "binding:loproc";
		case LIEF::ELF::SYMBOL_BINDINGS::STB_HIPROC: return "binding:hiproc";
		default: return "binding:unknown";
  		}
  	}

	asymtab_t* gen_from_elf_lief(char* path) {
		ANALOGif_debug(log_id, "Generating for path: " ANALOG_COLOR_VALUE("'%s'") "\n", path);
		asymtab_error_t err;
  		asymtab_t* symtab = asymtab_new(&err);
  		if (symtab == NULL) {
  			CURRENT_GEN_ERROR = asymtab_error_message(err);
			return NULL;
  		}
		std::unique_ptr<LIEF::ELF::Binary> elf {LIEF::ELF::Parser::parse(path)};
		ANALOGif_debug(log_id, "Symtab address: " ANALOG_COLOR_VALUE("0x%zx") "\n", symtab);
		ANALOGif_debug(log_id, "ELF object address: " ANALOG_COLOR_VALUE("0x%zx") "\n", elf.get());

		ANALOGif_debug(log_id, "Querying symbols\n");
		auto syms = elf->symbols();

		for (const LIEF::ELF::Symbol& elfsym : syms) {
			char* demangled_name = elfsym.demangled_name().c_str();
			size_t value = elfsym.value();

			char* type_str = type_to_str(elfsym.type());
			char* binding_str = binding_to_str(elfsym.binding());

			ANALOGif_debug(log_id, "Symbol at " ANALOG_COLOR_VALUE("0x%zx") " (type: " ANALOG_COLOR_VALUE("%s") ", binding: " ANALOG_COLOR_VALUE("%s") ")\n", value, type_str, binding_str);

			asymbol_t* sym = asymbol_new(demangled_name, value, "Automatically generated.", &err);
			if (sym == NULL) {
				CURRENT_GEN_ERROR = asymtab_error_message(err);
				return NULL;
			}

			asymbol_add_flag(sym, type_str);
			asymbol_add_flag(sym, binding_str);
        	asymtab_add(symtab, sym);
		}

		ANALOGif_debug(log_id, "Total number of symbols: " ANALOG_COLOR_VALUE("%zu") "\n", symtab->symbols_len);

		return symtab;
	}
}