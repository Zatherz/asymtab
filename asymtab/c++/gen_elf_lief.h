#ifndef H_GEN_ELF_LIEF
#define H_GEN_ELF_LIEF

#include <libasymtab/asymtab.h>

asymtab_t* gen_from_elf_lief(char* path);

#endif//H_GEN_ELF_LIEF