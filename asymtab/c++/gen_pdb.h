#ifndef H_GEN_PDB
#define H_GEN_PDB

#include <libasymtab/asymtab.h>

asymtab_t* gen_from_pdb(char* path);

#endif//H_GEN_PDB
