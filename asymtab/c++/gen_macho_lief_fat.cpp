#include <LIEF/LIEF.hpp>

extern "C" {
	#include "gen_macho_lief_fat.h"
	#include "../cli.h"
  	#include <libasymtab/asymtab.h>
  	#define SIZE_T_SPRINTF_BUFFER_LEN 20
  	// will be enough to hold all size_ts on a 64 bit machine
  	// (18446744073709551615 is max)

	struct fat_asymtab gen_from_macho_lief_fat(char* input_path, char* output_path) {
		asymtab_error_t err;
		std::unique_ptr<LIEF::MachO::FatBinary> fat_macho {LIEF::MachO::Parser::parse(input_path)};

		struct fat_asymtab output = {
			.symtabs = malloc(sizeof(asymtab_t*) * fat_macho->size()),
			.output_paths = malloc(sizeof(char*) * fat_macho->size()),
			.size = fat_macho->size()
		};

		// most of the code here is to extract the filename and extension
		// so that we can format the output nicely

		size_t path_len = strlen(output_path);
		char* last_char = output_path[path_len - 1];

		size_t ext_len = 0;
		// iterate from end
		for (char* ch = output_path + path_len - 1; ch >= output_path; ch--) {
			ext_len += 1;
			if (*ch == '.') {
				// extension ends here!

				break;
			}
		}

		char* ext = NULL;
		char* ext_begin = NULL;
		if (ext_len != path_len) {
			// extract the extension (include and reuse the \0 that'll be at the end of this string)

			ext_begin = output_path + path_len - ext_len;
			ext = malloc(sizeof(char) * (ext_len + 1));
			memcpy(ext, ext_begin, ext_len + 1);
		} else ext_len = 0;

		// extract the filename (this time we will have to add the \0 ourselves)
		size_t filename_len = path_len - ext_len;
		char* filename = malloc(sizeof(char) * (filename_len + 1));
		memcpy(filename, output_path, filename_len);
		filename[filename_len] = '\0';

		auto binaries = fat_macho->begin();

		if (fat_macho->size() > 1) {
			size_t index = 0;

			for (const LIEF::MachO::Binary& bin : binaries) {
				// @FIXME wrong assumption that fat binaries will only differ in cpu type?
				// I'm sure that's the most common usage, but if it's technically possible
				// to put in more, how do we even separate them?

				auto magic = bin.header().magic();

				char* output_subname = NULL;
				size_t output_subname_len = 4;
				// WARNING! OUTPUT SUBNAME HAS LENGTH OF 4 BY DEFAULT AS PER VARIABLE DEFINITION ABOVE!

				switch(magic) {
				case LIEF::MachO::MACHO_TYPES::MH_MAGIC: output_subname = ".32b"; break;
				case LIEF::MachO::MACHO_TYPES::MH_CIGAM: output_subname = ".32l"; break;
				case LIEF::MachO::MACHO_TYPES::MH_MAGIC_64: output_subname = ".64b"; break;
				case LIEF::MachO::MACHO_TYPES::MH_CIGAM_64: output_subname = ".64l"; break;
				default:
					output_subname_len = SIZE_T_SPRINTF_BUFFER_LEN + 8;
					output_subname = malloc(sizeof(char) * (output_subname_len + 1));
					break;
				}

		  		asymtab_t* symtab = asymtab_new(&err);
		  		if (symtab == NULL) {
		  			CURRENT_GEN_ERROR = asymtab_error_message(err);
					output.size = -1;
					return output;
		  		}

				output.symtabs[index] = symtab;
				output.output_paths[index] = malloc(sizeof(char) * (filename_len + output_subname_len + ext_len + 1));
				memcpy(output.output_paths[index], output_path, filename_len);
				memcpy(output.output_paths[index] + filename_len, output_subname, output_subname_len);
				if (ext != NULL) {
					memcpy(output.output_paths[index] + filename_len + output_subname_len, ext_begin, ext_len);
				}
				output.output_paths[index][filename_len + output_subname_len + ext_len] = '\0';

				auto syms = bin.symbols();
				for (const LIEF::MachO::Symbol& machosym : syms) {
				 	asymbol_t* sym = asymbol_new(machosym.demangled_name().c_str(), machosym.value(), "Automatically generated.", &err);
					if (sym == NULL) {
						CURRENT_GEN_ERROR = asymtab_error_message(err);
						output.size = -1;
						return output;
					}

	         		asymtab_add(symtab, sym);
			  	}

				index += 1;
			}
		} else {
			const LIEF::MachO::Binary& bin = *binaries; // deref iterator and get pointer to value
			asymtab_t* symtab = asymtab_new(&err);
			if (symtab == NULL) {
				CURRENT_GEN_ERROR = asymtab_error_message(err);
				output.size = -1;
				return output;
			}

			auto syms = bin.symbols();
			for (const LIEF::MachO::Symbol& machosym : syms) {
			 	asymbol_t* sym = asymbol_new(machosym.demangled_name().c_str(), machosym.value(), "Automatically generated.", &err);
			 	if (sym == NULL) {
					CURRENT_GEN_ERROR = asymtab_error_message(err);
					output.size = -1;
					return output;
				}
         		asymtab_add(symtab, sym);
		  	}

		  	output.symtabs[0] = symtab;
		  	output.output_paths[0] = output_path;
		}

		// auto syms = fat_macho->symbols();

		// for (const LIEF::MachO::Symbol& machosym : syms) {
		// 	asymbol_t* sym = asymbol_new(machosym.demangled_name().c_str(), machosym.value(), "Automatically generated.");
  //       	asymtab_add(symtab, sym);
		// }

		return output;
	}
}