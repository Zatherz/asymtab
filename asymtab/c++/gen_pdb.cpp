#include <pdbparser/pdb_file.h>
#include <pdbparser/pdb_symbols.h>

using namespace pdbparser;

extern "C" {
  #include "gen_pdb.h"
  #include "../cli.h"
  #include <libasymtab/asymtab.h>
  #include <libanalog/akit_colors.h>

  static const char* log_id = ANALOG_COLOR_ID("pdb");

  const char* msg_for_pdb_state(PDBFileState state) {
    if (state == PDBFileState::PDB_STATE_OK) return NULL;
    else if (state == PDB_STATE_UNSUPPORTED_VERSION) return "Unsupported PDB version";
    else if (state == PDB_STATE_ALREADY_LOADED) return "PDB file is already loaded";
    else if (state == PDB_STATE_INVALID_FILE) return "The PDB file is invalid or corrupted";
    else if (state == PDB_STATE_ERR_FILE_OPEN) return "Failed opening the PDB file";
    return "Unknown error while loading the PDB";
  }

  asymtab_t* gen_from_pdb(char* path) {
    ANALOGif_debug(log_id, "Generating for path: " ANALOG_COLOR_VALUE("'%s'") "\n", path);

    asymtab_error_t err;
    asymtab_t* symtab = asymtab_new(&err);
    if (symtab == NULL) {
      CURRENT_GEN_ERROR = asymtab_error_message(err);
    return NULL;
    }

    ANALOGif_debug(log_id, "Symtab address: " ANALOG_COLOR_VALUE("0x%zx") "\n", symtab);

    auto file = new PDBFile();

    ANALOGif_debug(log_id, "PDB object address: " ANALOG_COLOR_VALUE("0x%zx") "\n", file);

    auto pdberr = msg_for_pdb_state(file->load_pdb_file(path));
    if (pdberr != NULL) {
      CURRENT_GEN_ERROR = pdberr;
      return NULL;
    }
    file->initialize(0);

    auto syms = file->get_symbols_container();
    ANALOGif_debug(log_id, "Symbols container address: " ANALOG_COLOR_VALUE("0x%zx") "\n", syms);
    if (syms == NULL) {
      CURRENT_GEN_ERROR = "Failed getting the symbols container";
      return NULL;
    }
    syms->parse_symbols();

    auto funcs = syms->get_functions();

    for (auto const &ent : funcs) {
      // TODO
      // use even more pdb metadata

      size_t addr = (size_t)ent.first;

      asymbol_t* sym = asymbol_new(ent.second->name, addr, (char*)"Automatically generated.", &err);
      if (sym == NULL) {
        CURRENT_GEN_ERROR = asymtab_error_message(err);
        return NULL;
      }

      char* type_def = ent.second->type_def->to_llvm().c_str();

      size_t prefix_len = strlen("returns:");
      size_t type_def_len = strlen(type_def);
      size_t ret_type_flag_len = prefix_len + type_def_len;
      char* ret_type_flag = malloc(sizeof(char) * (ret_type_flag_len + 1));
      strncpy(ret_type_flag, "returns:", prefix_len);
      strncpy(ret_type_flag + prefix_len, type_def, type_def_len);
      ret_type_flag[ret_type_flag_len] = '\0';

      for(auto arg : ent.second->arguments) {
        char* arg_type_def = arg.type_def->to_llvm().c_str();
        char* arg_name = arg.name;

        size_t arg_name_len = strlen(arg_name);
        size_t arg_prefix_len = strlen("arg:");
        size_t arg_type_def_len = strlen(arg_type_def);

        size_t arg_flag_len = arg_prefix_len + arg_name_len + 1 + arg_type_def_len;

        char* arg_flag = malloc(sizeof(char) * (arg_flag_len + 1));
        strncpy(arg_flag, "arg:", arg_prefix_len);
        strncpy(arg_flag + arg_prefix_len, arg_name, arg_name_len);
        strncpy(arg_flag + arg_prefix_len + arg_name_len, "=", 1);
        strncpy(arg_flag + arg_prefix_len + arg_name_len + 1, arg_type_def, arg_type_def_len);
        arg_flag[arg_flag_len] = '\0';

        ANALOGif_debug("Argument: " ANALOG_COLOR_VALUE("'%s'") " = " ANALOG_COLOR_VALUE("'%s'") "\n", arg_name, arg_type_def);

        asymbol_add_flag(sym, arg_flag);
      }

      asymbol_add_flag(sym, ret_type_flag);

      ANALOGif_debug(log_id, "Symbol at " ANALOG_COLOR_VALUE("0x%zx") " (%s)\n", addr, ret_type_flag);

      asymtab_add(symtab, sym);
    }

    ANALOGif_debug(log_id, "Total number of symbols: " ANALOG_COLOR_VALUE("%zu") "\n", symtab->symbols_len);


    return symtab;
  }
}
