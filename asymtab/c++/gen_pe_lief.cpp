#include <LIEF/LIEF.hpp>

extern "C" {
	#include "gen_pe_lief.h"
	#include "../cli.h"
  	#include <libasymtab/asymtab.h>

	asymtab_t* gen_from_pe_lief(char* path) {
  		asymtab_error_t err;
  		asymtab_t* symtab = asymtab_new(&err);
  		if (symtab == NULL) {
  			CURRENT_GEN_ERROR = asymtab_error_message(err);
			return NULL;
  		}
		std::unique_ptr<LIEF::PE::Binary> pe {LIEF::PE::Parser::parse(path)};

		//std::cout << "PE BINARY: " << *pe << std::endl;

		auto syms = pe->symbols();

		for (const LIEF::PE::Symbol& pesym : syms) {
			asymbol_t* sym = asymbol_new(pesym.name().c_str(), pesym.value(), "Automatically generated.", &err);
			if (sym == NULL) {
				CURRENT_GEN_ERROR = asymtab_error_message(err);
				return NULL;
			}
        	asymtab_add(symtab, sym);
		}

		return symtab;
	}
}