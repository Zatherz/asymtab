#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include "cli.h"
#include <libasymtab/asymtab.h>
#include <libanalog/analog.h>
#include <libanalog/akit_colors.h>
#include "c++/gen_pdb.h"
#include "c++/gen_elf_lief.h"
#include "c++/gen_pe_lief.h"
#include "c++/gen_macho_lief_fat.h"

const char* CURRENT_GEN_ERROR = NULL;
bool debug_enabled = false;

ANALOG_DEFINE_LOG_TYPE(debug, ANALOG_COLOR_DEBUG, ANALOG_COLOR_DEBUG_TEXT);
ANALOG_DEFINE_LOG_TYPE(error, ANALOG_COLOR_ERROR, ANALOG_COLOR_ERROR_TEXT);
ANALOG_DEFINE_LOG_TYPE(tip, ANALOG_COLOR_TIP, ANALOG_COLOR_TIP_TEXT);

bool log_filter(char* log_type) {
  if (strcmp(log_type, "debug") == 0) return debug_enabled;
  return true;
}

struct subcommand gen_subcommands[] = {
  {"elf", "<input> [output]", &command_elf, "Generates an asymtab from the symbol table of an unstripped ELF file."},
  {"pe", "<input> [output]", &command_pe, "Generates an asymtab from the public symbol table of an unstripped PE file."},
  {"pdb", "<input> [output]", &command_pdb, "Generates an asymtab from the symbols defined in a Microsoft PDB file."},
  {"macho", "<input> [output]", &command_macho, "Generates an asymtab from the symbols defined in a MachO binary (both fat and not)."},
  {NULL, NULL, NULL, NULL}
};

struct subcommand aux_subcommands[] = {
  {"help", "[command|gen [subcommand]]", &command_help, "Shows help."},
  {"detail", "<symtab> <name>", &command_detail, "Get the address and description of a symbol with the specified name."},
  {"name", "<symtab> <hexaddr>", &command_name, "Get the name of a symbol with the specified address."},
  {"addr", "<symtab> <name>", &command_addr, "Get the address of a symbol with the specified name."},
  {"grep", "<symtab> <filter> [filter [filter...]]", &command_grep, "Search for symbols in a symbol table using fragments of or full names, addresses, flags and descriptions."},
  {NULL, NULL, NULL, NULL}
};

void handle_symtab(asymtab_t* symtab, char* output_path) {
  if (symtab == NULL) {
    ANALOGf_error("Failed generating the symbol table: %s\n", CURRENT_GEN_ERROR);
    exit(1);
  }

  ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Serializing the symbol table.\n"));
  sds serialized = asymtab_serialize(symtab);

  FILE* output = fopen(output_path, "wb");
  if (output == NULL) {
    ANALOGf_error("Failed opening output file for writing\n");
    exit(1);
  }

  fwrite(serialized, 1, sdslen(serialized), output);
  fclose(output);

  ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Freeing the symbol table.\n"));
  sdsfree(serialized);
  asymtab_free_with_symbols(symtab);
}

COMMAND(elf) {
  if (argc < 1) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  char* input_path = argv[0];
  size_t input_len = strlen(input_path);
  char* output_path;
  if (argc >= 2) {
    output_path = argv[1];
  } else {
    output_path = malloc(sizeof(char) * (input_len + 5 + 1));
    memcpy(output_path, input_path, input_len);
    memcpy(output_path + input_len, ".syms", 5);
    output_path[input_len + 5] = '\0';
  }

  ANALOGf_debug("Input: " ANALOG_COLOR_VALUE("'%s'") "\n", input_path);
  ANALOGf_debug("Output: " ANALOG_COLOR_VALUE("'%s'") "\n", output_path);

  asymtab_t* symtab = gen_from_elf_lief(input_path);
  handle_symtab(symtab, output_path);
}

COMMAND(pe) {
  if (argc < 1) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  char* input_path = argv[0];
  size_t input_len = strlen(input_path);
  char* output_path;
  if (argc >= 2) {
    output_path = argv[1];
  } else {
    output_path = malloc(sizeof(char) * (input_len + 5 + 1));
    memcpy(output_path, input_path, input_len);
    memcpy(output_path + input_len, ".syms", 5);
    output_path[input_len + 5] = '\0';
  }

  ANALOGf_debug("Input: " ANALOG_COLOR_VALUE("'%s'") "\n", input_path);
  ANALOGf_debug("Output: " ANALOG_COLOR_VALUE("'%s'") "\n", output_path);

  asymtab_t* symtab = gen_from_pe_lief(input_path);
  handle_symtab(symtab, output_path);
}

COMMAND(pdb) {
  if (argc < 1) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  char* input_path = argv[0];
  size_t input_len = strlen(input_path);
  char* output_path;
  if (argc >= 2) {
    output_path = argv[1];
  } else {
    output_path = malloc(sizeof(char) * (input_len + 5 + 1));
    memcpy(output_path, input_path, input_len);
    memcpy(output_path + input_len, ".syms", 5);
    output_path[input_len + 5] = '\0';
  }

  ANALOGf_debug("Input: " ANALOG_COLOR_VALUE("'%s'") "\n", input_path);
  ANALOGf_debug("Output: " ANALOG_COLOR_VALUE("'%s'") "\n", output_path);

  asymtab_t* symtab = gen_from_pdb(input_path);
  handle_symtab(symtab, output_path);
}

COMMAND(macho) {
  if (argc < 1) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  char* input_path = argv[0];
  size_t input_len = strlen(input_path);
  char* output_path;
  if (argc >= 2) {
    output_path = argv[1];
  } else {
    output_path = malloc(sizeof(char) * (input_len + 5 + 1));
    memcpy(output_path, input_path, input_len);
    memcpy(output_path + input_len, ".syms", 5);
    output_path[input_len + 5] = '\0';
  }

  ANALOGf_debug("Input: " ANALOG_COLOR_VALUE("'%s'") "\n", input_path);
  ANALOGf_debug("Output: " ANALOG_COLOR_VALUE("'%s'") "\n", output_path);

  struct fat_asymtab output = gen_from_macho_lief_fat(input_path, output_path);
  if (output.size == -1) handle_symtab(NULL, NULL);
  else {
    for (size_t i = 0; i < output.size; i++) {
      handle_symtab(output.symtabs[i], output.output_paths[i]);
    }
  }
  // handle_symtab(symtab, output_path);
}

char* downcase(char* str) {
  if (str == NULL) return NULL;

  size_t len = strlen(str);
  char* dest = malloc(sizeof(char) * (len + 1));

  for (size_t i = 0; i < len; i++) {
    dest[i] = tolower(str[i]);
  }

  dest[len] = '\0';

  return dest;
}

void print_symbol(asymbol_t* sym) {
  printf(ANALOG_ANSI_FG_BRIGHT_YELLOW);
  if (strlen(sym->name) == 0) printf("[no name]");
  else printf("'%s'", sym->name);
  printf(ANALOG_ANSI_RESET);

  printf(" = ");

  printf(ANALOG_ANSI_FG_BRIGHT_YELLOW);
  printf("0x%zx\n", sym->address);
  printf(ANALOG_ANSI_RESET);

  if (sym->flags != NULL) {
    for (size_t i = 0; i < sym->flags_len; i++) {
      char* flag = sym->flags[i];

      char* cpy = malloc(sizeof(char) * (strlen(flag) + 1));
      strcpy(cpy, flag);
      cpy[strlen(flag)] = '\0';

      char* sep = strchr(cpy, ':');

      bool split = false;
      char* first_el = cpy;
      char* second_el = NULL;
      if (sep != NULL) {
        split = true;
        *sep = '\0';
        second_el = sep + 1;
      }

      printf("  * ");

      if (split) {
        printf(ANALOG_ANSI_FG_BRIGHT_CYAN);
        printf("%s", first_el);
        printf(ANALOG_ANSI_RESET);
        printf(": ");
        printf(ANALOG_ANSI_FG_BRIGHT_BLUE);
        printf("%s\n", second_el);
        printf(ANALOG_ANSI_RESET);
      } else {
        printf(ANALOG_ANSI_FG_BRIGHT_CYAN);
        printf("%s\n", sym->flags[i]);
        printf(ANALOG_ANSI_RESET);
      }
    }
  }

  printf("  %s\n", sym->description);
}

COMMAND(grep) {
  if (argc < 2) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  char* path = argv[0];

  // shift
  argc -= 1;
  argv += 1;

  asymtab_error_t err;
  asymtab_t* symtab = asymtab_read(path, &err);

  if (symtab == NULL) {
    ANALOGf_error("Failed reading symbol table at path '%s': %s.\n", path, asymtab_error_message(err));
    exit(1);
  }

  ANALOGf_debug("Queries:\n");
  for (int i = 0; i < argc; i++) {
    ANALOGf_debug("  - " ANALOG_COLOR_VALUE("'%s'") "\n", argv[i]);
  }

  for (size_t i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* sym = symtab->symbols[i];

    char* lowercase_name = downcase(sym->name);
    char* lowercase_desc = downcase(sym->description);

    bool all_match = true;
    for (int i = 0; i < argc; i++) {
      bool matches = false;

      char* query = downcase(argv[i]);

      if (sym->flags != NULL) {
        for (size_t j = 0; j < sym->flags_len; j++) {
          if (strstr(sym->flags[j], query) != NULL) {
            matches = true;
            break;
          }
        }
      }

      char address_str[MAX_SIZE_HEX_DIGITS + 2] = "";
      snprintf(address_str, MAX_SIZE_HEX_DIGITS + 2, "0x%zx", sym->address);

      matches = matches || strstr(address_str, query) != NULL;
      matches = matches || strstr(lowercase_name, query) != NULL;
      matches = matches || strstr(lowercase_desc, query) != NULL;

      if (!matches) {
        all_match = false;
        break;
      }
    }

    if (all_match) {
      print_symbol(sym);
    }

    free(lowercase_name);
    free(lowercase_desc);
  }

  asymtab_free_with_symbols(symtab);
}

COMMAND(addr) {
  if (argc < 2) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  asymtab_error_t err;
  asymtab_t* symtab = asymtab_read(argv[0], &err);

  if (symtab == NULL) {
    ANALOGf_error("Failed reading symbol table at path '%s': %s.\n", argv[0], asymtab_error_message(err));
    exit(1);
  }

  bool found = false;

  for (int i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* sym = symtab->symbols[i];

    if (strcmp(sym->name, argv[1]) == 0) {
      printf("0x%zx\n", sym->address);
      found = true;
      break;
    }
  }

  asymtab_free_with_symbols(symtab);

  if (!found) {
    ANALOGf_error("Symbol doesn't exist in the symbol table\n");
    exit(1);
  }
}

COMMAND(detail) {
  if (argc < 2) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  asymtab_error_t err;
  asymtab_t* symtab = asymtab_read(argv[0], &err);

  if (symtab == NULL) {
    ANALOGf_error("Failed reading symbol table at path '%s': %s.\n", argv[0], asymtab_error_message(err));
    exit(1);
  }

  bool found = false;

  for (int i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* sym = symtab->symbols[i];

    if (strcmp(sym->name, argv[1]) == 0) {
      print_symbol(sym);
      found = true;
      break;
    }
  }

  asymtab_free_with_symbols(symtab);

  if (!found) {
    ANALOGf_error("Symbol doesn't exist in the symbol table\n");
    exit(1);
  }
}

COMMAND(name) {
  if (argc < 2) {
    ANALOGf_error("Too few arguments!\n");
    exit(1);
  }

  asymtab_error_t err;
  asymtab_t* symtab = asymtab_read(argv[0], &err);

  if (symtab == NULL) {
    ANALOGf_error("Failed reading symbol table at path '%s': %s.\n", argv[0], asymtab_error_message(err));
    exit(1);
  }

  bool found = false;

  char* addr_str = argv[1];
  size_t addr;
  if (!sscanf(addr_str, "0x%zx", &addr)) {
    ANALOGf_error("Invalid input address\n");
    exit(1);
  }

  for (int i = 0; i < symtab->symbols_len; i++) {
    asymbol_t* sym = symtab->symbols[i];

    if (addr == sym->address) {
      puts(sym->name);
      found = true;
    }
  }

  asymtab_free_with_symbols(symtab);

  if (!found) {
    ANALOGf_error("Symbol doesn't exist in the symbol table\n");
    exit(1);
  } 
}

COMMAND(help) {
  switch (argc) {
  case 0:
    puts("general usage: asymtab [-d] <command> [args...]");
    for (int i = 0; aux_subcommands[i].name != NULL; i++) {
      fputs("       asymtab ", stdout);
      fputs(aux_subcommands[i].name, stdout);
      fputs(" ", stdout);
      puts(aux_subcommands[i].short_usage);
      fputs("           ", stdout);
      puts(aux_subcommands[i].desc);
    }
    puts("");
    puts("symtab generation: asymtab [-d] gen <target> [args...]");
    for (int i = 0; gen_subcommands[i].name != NULL; i++) {
      fputs("       asymtab gen ", stdout);
      fputs(gen_subcommands[i].name, stdout);
      fputs(" ", stdout);
      puts(gen_subcommands[i].short_usage);
      fputs("           ", stdout);
      puts(gen_subcommands[i].desc);
    }
    break;
  case 1:
    if (strcmp(argv[0], "gen") == 0) {
      puts("usage: asymtab [-d] gen <target> [args...]");
      for (int i = 0; gen_subcommands[i].name != NULL; i++) {
        fputs("       asymtab gen ", stdout);
        fputs(gen_subcommands[i].name, stdout);
        fputs(" ", stdout);
        puts(gen_subcommands[i].short_usage);
        fputs("           ", stdout);
        puts(gen_subcommands[i].desc);
      }
      return;
    } else {
      for (int i = 0; aux_subcommands[i].name != NULL; i++) {
        if (strcmp(aux_subcommands[i].name, argv[0]) == 0) {
          fputs("usage: asymtab [-d] ", stdout);
          fputs(argv[0], stdout);
          fputs(" ", stdout);
          puts(aux_subcommands[i].short_usage);
          printf("       %s\n", aux_subcommands[i].desc);
          return;
        }
      }
      for (int i = 0; gen_subcommands[i].name != NULL; i++) {
        if (strcmp(gen_subcommands[i].name, argv[0]) == 0) {
          fputs("usage: asymtab [-d] gen ", stdout);
          fputs(argv[0], stdout);
          fputs(" ", stdout);
          puts(gen_subcommands[i].short_usage);
          puts("");
          puts(gen_subcommands[i].desc);
          return;
        }
      }
    }
    fputs("Subcommand doesn't exist!\n", stderr);
    break;
  case 2:
    if (strcmp(argv[0], "gen") == 0) {
      argc -= 1;
      argv += 1;
      for (int i = 0; gen_subcommands[i].name != NULL; i++) {
        if (strcmp(gen_subcommands[i].name, argv[0]) == 0) {
          fputs("usage: asymtab gen ", stdout);
          fputs(argv[0], stdout);
          fputs(" ", stdout);
          puts(gen_subcommands[i].short_usage);
          puts("");
          puts(gen_subcommands[i].desc);
          return;
        }
      }
    }
    fputs("Subcommand doesn't exist!\n", stderr);
    break;
  default:
    fputs("Too many arguments!\n", stderr);
    exit(1);
    break;
  }
}

int main(int argc, char** argv) {
  ANALOG_CONFIG_default_id = ANALOG_COLOR_ID("asymtab");
  ANALOG_CONFIG_filter = log_filter;

  if (argc < 2) {
    command_help(0, NULL);
    return 1;
  }

  char* cmd_name = argv[1];
  if (strcmp(cmd_name, "-d") == 0) {
    debug_enabled = true;
    cmd_name = argv[2];
    argc -= 1;
    argv += 1;
  }

  if (argc <= 1) {
    command_help(0, NULL);
    return 1;
  }

  ANALOGf_debug("Command: " ANALOG_COLOR_VALUE("%s") "\n", cmd_name);

  argc -= 2;
  argv += 2;

  

  if (strcmp(cmd_name, "gen") == 0) {
    if (argc == 0) {
      command_help(0, NULL);
      return 1;
    }

    char* subcmd_name = argv[0];
    ANALOGf_debug("Target: " ANALOG_COLOR_VALUE("%s") "\n", subcmd_name);

    argc -= 1;
    argv += 1;

    for (int i = 0; gen_subcommands[i].name != NULL; i++) {
      if (strcmp(gen_subcommands[i].name, subcmd_name) == 0) {
        ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Calling generator.\n"));
        gen_subcommands[i].func(argc, argv);
        return 0;
      }
    }
  } else {
    for (int i = 0; aux_subcommands[i].name != NULL; i++) {
      if (strcmp(aux_subcommands[i].name, cmd_name) == 0) {
        ANALOGf_debug(ANALOG_COLOR_TOP_LEVEL_GOAL("Running command.\n"));
        aux_subcommands[i].func(argc, argv);
        return 0;
      }
    }
  }

  command_help(0, NULL);
  return 1;
}

