#ifndef H_ASYMTAB_CLI
#define H_ASYMTAB_CLI
#include <stdlib.h>
#include <libanalog/analog.h>

/*
  ©2018 Zatherz
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

ANALOG_DECLARE_LOG_TYPE(debug);
ANALOG_DECLARE_LOG_TYPE(error);
ANALOG_DECLARE_LOG_TYPE(tip);

#define MAX_SIZE_HEX_DIGITS 14

extern const char* CURRENT_GEN_ERROR;

struct subcommand {
  char* name;
  char* short_usage;
  void (*func)(int argc, char** argv);
  char* desc;
};

#define COMMAND(x) void command_ ## x(int argc, char** argv)

COMMAND(help);
COMMAND(addr);
COMMAND(detail);
COMMAND(name);
COMMAND(grep);

// generation method
COMMAND(elf);
COMMAND(elfasm);
COMMAND(pe);
COMMAND(pdb);
COMMAND(macho);

#endif//H_ASYMTAB_CLI
