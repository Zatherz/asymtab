asymtab
===

asymtab is a **highly WIP** CLI interface to [libasymtab](https://gitlab.com/Zatherz/libasymtab), featuring various methods of generating compatible symbol tables (for example, from ELF symbol table data or PDB debug data, or by making an attempt at automatically detecting functions in the assembly).

# Language

asymtab, just like libasymtab and [autopatch](https://gitlab.com/Zatherz/autopatch) is written in C. However, asymtab is also partially written in C++ (majority of available generators) and depends on the C++ standard library.

# Building

## Linux

  mkdir build
  cd build
  cmake ..
  make

## Windows
  
  mkdir build
  cd build
  cmake -DWINDOWS=1 ..
  make

## Cross compiling for Windows

  mkdir build
  cd build
  cmake -DWINDOWS=1 \
        -DCMAKE_SYSTEM_NAME=Windows \
        -DCMAKE_C_COMPILER=x86_64-w64-mingw32-gcc \
        -DCMAKE_CXX_COMPILER=x86_64-w64-mingw32-g++ \
        -DCMAKE_RANLIB=/usr/bin/x86_64-w64-mingw32-ranlib \
        -DTESTCMD="env WINEPATH='.\\libasymtab\\libasymtab' wine" \
        .. # TESTCMD is for libasymtab
  make

# License

asymtab is licensed under the MIT license. It makes use of `pdbparser`, licensed under MIT, and `LIEF`, licensed under Apache 2.0. See `NOTICE`.

# Usage

Run `asymtab help` for a list of available commands.

```
general usage: asymtab <command> [args...]
       asymtab help [command|gen [subcommand]]
           Shows help.
       asymtab detail <symtab> <name>
           Get the address and description of a symbol with the specified name.
       asymtab name <symtab> <hexaddr>
           Get the name of a symbol with the specified address.
       asymtab addr <symtab> <name>
           Get the address of a symbol with the specified name.
       asymtab grep <symtab> <query>
           Search for symbols in a symbol table using fragments or the entirety of names, addresses and descriptions.

symtab generation: asymtab gen <command> [args...]
       asymtab gen elf <input> [output]
           Generates an asymtab from the symbol table of an unstripped ELF file.
       asymtab gen pe <input> [output]
           Generates an asymtab from the public symbol table of an unstripped PE file.
       asymtab gen pdb <input> [output]
           Generates an asymtab from the symbols defined in a Microsoft PDB file.
       asymtab gen macho <input> [output]
           Generates an asymtab from the symbols defined in a MachO binary (both fat and not).
```

# Generating symbol tables

The main purpose of asymtab is to generate libasymtab compatible symbol tables. You can do this through the use of `gen` subcommands like `elf` or `pdb`. For example:

    asymtab gen elf asymtab

The above will create a file `asymtab.syms` in the libasymtab format using data present in the ELF symtab section. Other available generator commands include `pe`, `pdb` and `macho`. Use `asymtab help gen` for a full list.

More methods will eventually be added and the currently available ones will be improved.

# Manipulating symbol tables

Apart from generating symtabs, you can also view and query information in them. For example, you can get the address of a symbol:

    asymtab addr asymtab.syms main

Or get the name from an address:

	asymtab name asymtab.syms 0xdeadbeef

# Available generation methods

* `elf` - extracts the ELF symtab section (if it's available, i.e., if the binary isn't stripped; C++ method names are demangled)
* `pe` - extracts the exported symbols from a PE binary
* `pdb` - extracts function symbols from a Microsoft PDB (program database) file
* `macho` - extracts symbols from the symtab of a MachO binary (this also works on fat binaries and will create a file per bundled binary; C++ method names are demangled)

# TODO

* "Boring" option for the asm generator (so that instead of random adjectives and nouns it produces something like `func_1845235`)
* Sanitization for names
* Interactive interface (especially for editing the symbol table file)
* Improved function detection (for `*_asm` generators)
* Automatic function detection generators for PE binaries (`pe_asm`)
* Support for easy 32 bit/Windows cross compilation (like libasymtab)
* macOS support
